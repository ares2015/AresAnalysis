import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@EnableAutoConfiguration
public class AresAnalysis {

    public static void main(String[] args){
        SpringApplication.run(AresAnalysis.class, args);
    }

    @RequestMapping(path = "/getDataByVerbRelation", method = RequestMethod.GET, produces = {MediaType.TEXT_PLAIN_VALUE})
    String getDataByVerbRelation(@RequestParam(value = "verbRelation") String verbRelation) {
        final String uri = "http://projectares.sk/ares_news_analysis/cloud/news_semantic_data.php?mode=chunks&verb_predicate="+verbRelation;
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri, String.class);
    }

}
